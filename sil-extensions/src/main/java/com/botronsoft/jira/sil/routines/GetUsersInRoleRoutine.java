package com.botronsoft.jira.sil.routines;

import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleActors;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.keplerrominfo.common.util.MutableString;
import com.keplerrominfo.common.util.StringUtils;
import com.keplerrominfo.jira.commons.ivm.routines.admin.GetProjectComponentsRoutine;
import com.keplerrominfo.sil.lang.AbstractRoutine;
import com.keplerrominfo.sil.lang.KeyableArraySILObject;
import com.keplerrominfo.sil.lang.SILContext;
import com.keplerrominfo.sil.lang.SILException;
import com.keplerrominfo.sil.lang.SILType;
import com.keplerrominfo.sil.lang.SILValue;
import com.keplerrominfo.sil.lang.SILValueFactory;
import com.keplerrominfo.sil.lang.type.TypeInst;

public class GetUsersInRoleRoutine extends AbstractRoutine<KeyableArraySILObject<MutableString>> {

	private static final Log LOG = LogFactory.getLog(GetProjectComponentsRoutine.class);
	private static final SILType<KeyableArraySILObject<MutableString>> RET_TYPE = TypeInst.STRING_ARR;
	private static SILType[] TYPES = { TypeInst.STRING, TypeInst.STRING };

	public GetUsersInRoleRoutine(String name) {
		super(name, TYPES);
	}

	@Override
	protected SILValue<KeyableArraySILObject<MutableString>> executeRoutine(SILContext paramSILContext,
			List<SILValue<?>> params) {
		String key = StringUtils.trim(((SILValue) params.get(0)).toStringValue());
		if (key == null) {
			String msg = "Null key provided for the project. Doesn't work like that!";
			LOG.error(msg);
			throw new SILException(msg);
		}

		Project prj = ComponentAccessor.getProjectManager().getProjectObjByKey(key);
		if (prj == null) {
			String msg = String.format("Project key '%s' does not represent a project.!", new Object[] { key });
			LOG.error(msg);
			throw new SILException(msg);
		}
		
		String roleName = StringUtils.trim(((SILValue) params.get(1)).toStringValue());
		if (roleName == null) {
			String msg = "Null name provided for the role. Doesn't work like that!";
			LOG.error(msg);
			throw new SILException(msg);
		}

		ProjectRoleManager projectRoleManager = ComponentAccessor.getComponent(ProjectRoleManager.class);
		ProjectRole projectRole = projectRoleManager.getProjectRole(roleName);
		if (projectRole == null) {
			String msg = String.format("Role name '%s' does not represent a project role.!", new Object[] { roleName });
			LOG.error(msg);
			throw new SILException(msg);
		}
		
		
		ProjectRoleActors actors = projectRoleManager.getProjectRoleActors(projectRole, prj);
		Collection<String> usernames = Collections2.transform(actors.getApplicationUsers(), new Function<ApplicationUser, String>() {

			@Override
			public String apply(ApplicationUser input) {
				return input.getUsername();
			}
		});
		return SILValueFactory.stringArray(usernames);
	}

	@Override
	public SILType<KeyableArraySILObject<MutableString>> getReturnType() {
		return RET_TYPE;
	}

	@Override
	public String getParams() {
		return "(pkey, role)";
	}

}
