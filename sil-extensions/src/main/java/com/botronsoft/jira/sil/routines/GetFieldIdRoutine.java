package com.botronsoft.jira.sil.routines;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.botronsoft.jira.sil.FieldResolver;
import com.keplerrominfo.common.util.MutableString;
import com.keplerrominfo.common.util.StringUtils;
import com.keplerrominfo.sil.lang.AbstractRoutine;
import com.keplerrominfo.sil.lang.SILContext;
import com.keplerrominfo.sil.lang.SILException;
import com.keplerrominfo.sil.lang.SILType;
import com.keplerrominfo.sil.lang.SILValue;
import com.keplerrominfo.sil.lang.SILValueFactory;
import com.keplerrominfo.sil.lang.type.TypeInst;

public class GetFieldIdRoutine extends AbstractRoutine<MutableString> {
	private static final Log LOG = LogFactory.getLog(GetFieldIdRoutine.class);

	private static SILType[] types = { TypeInst.STRING };

	public GetFieldIdRoutine(String name) {
		super(name, types, false);
	}

	@SuppressWarnings("unchecked")
	protected SILValue<MutableString> executeRoutine(SILContext ctx, List<SILValue<?>> params) {
		String key = StringUtils.trim(((SILValue) params.get(0)).toStringValue());
		if (key == null) {
			String msg = "Null key provided for the field. Doesn't work like that!";
			LOG.error(msg);
			throw new SILException(msg);
		}

		//
		return SILValueFactory.string(FieldResolver.getFieldId(key));
	}

	public String getParams() {
		return "(\"field\")";
	}

	public SILType<MutableString> getReturnType() {
		return TypeInst.STRING;
	}

}
