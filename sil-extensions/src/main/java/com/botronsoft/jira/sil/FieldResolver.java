package com.botronsoft.jira.sil;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.keplerrominfo.jira.commons.ivm.IssueFields;
import com.keplerrominfo.sil.lang.SILException;

public class FieldResolver {
	private static final Log LOG = LogFactory.getLog(FieldResolver.class);

	public static String getFieldId(String fieldName, String issueKey) {
		final MutableIssue issue = ComponentAccessor.getIssueManager().getIssueObject(issueKey);
		if (issue == null) {
			String msg = String.format("Issue with key %s could not be found when resolving field %s", issueKey,
					fieldName);
			LOG.debug(msg);
			throw new SILException(msg);
		}
		return getFieldId(fieldName, issue);
	}

	public static String getFieldId(String fieldName, final MutableIssue issue) {
		IssueFields issueFields = ComponentAccessor.getComponent(IssueFields.class);
		if (!issueFields.isStandardField(fieldName)) {
			Collection<CustomField> fields = ComponentAccessor.getCustomFieldManager()
					.getCustomFieldObjectsByName(fieldName);
			if (fields.size() > 1) {

				CustomField field = Iterables.tryFind(fields, new Predicate<CustomField>() {
					@Override
					public boolean apply(CustomField input) {
						return input.isInScope(issue.getProjectId(), issue.getIssueTypeId());
					}
				}).orNull();
				if (field != null) {
					return field.getId();
				}
				String msg = String.format(
						"Field >>%s<< cannot be matched against a custom field, in the context of issue with key %s",
						fieldName, issue.getKey());
				LOG.debug(msg);
				throw new SILException(msg);

			}
		}
		return issueFields.normalizeName(fieldName);
	}

	public static String getFieldId(String fieldName) {
		IssueFields issueFields = ComponentAccessor.getComponent(IssueFields.class);
		if (!issueFields.isStandardField(fieldName)) {
			int numberOfFieldsNamed = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName(fieldName)
					.size();
			if (numberOfFieldsNamed > 1) {
				LOG.warn(String.format("There are %s fields named %s. This might cause unexpected results.",
						numberOfFieldsNamed, fieldName));
			}
		}
		return issueFields.normalizeName(fieldName);
	}
}
