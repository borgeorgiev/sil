package com.botronsoft.jira.sil;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import com.botronsoft.jira.sil.routines.CloneToSubtask;
import com.botronsoft.jira.sil.routines.GetFieldIdRoutine;
import com.botronsoft.jira.sil.routines.GetUsersInRoleRoutine;
import com.botronsoft.jira.sil.routines.VerifyFieldChangedDuringTransitionRoutine;
import com.keplerrominfo.sil.lang.RoutineRegistry;

public class ExtensionsServiceImpl implements ExtensionsService, InitializingBean, DisposableBean {

	@Override
	public void destroy() throws Exception {
		// RoutineRegistry.unregister(r);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		RoutineRegistry.register(new GetUsersInRoleRoutine("roleUsers"));
		RoutineRegistry.register(new VerifyFieldChangedDuringTransitionRoutine("fieldChanged"));
		RoutineRegistry.register(new GetFieldIdRoutine("getFieldId"));
		RoutineRegistry.register(new CloneToSubtask("cloneIssueToSubtask"));
	}

}
