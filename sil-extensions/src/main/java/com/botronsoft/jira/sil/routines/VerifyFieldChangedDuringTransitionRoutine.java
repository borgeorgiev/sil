package com.botronsoft.jira.sil.routines;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.MutableIssue;
import com.botronsoft.jira.sil.FieldResolver;
import com.google.common.base.Objects;
import com.keplerrominfo.common.util.MutableBoolean;
import com.keplerrominfo.common.util.StringUtils;
import com.keplerrominfo.sil.lang.AbstractRoutine;
import com.keplerrominfo.sil.lang.SILContext;
import com.keplerrominfo.sil.lang.SILException;
import com.keplerrominfo.sil.lang.SILType;
import com.keplerrominfo.sil.lang.SILValue;
import com.keplerrominfo.sil.lang.SILValueFactory;
import com.keplerrominfo.sil.lang.type.TypeInst;

import webwork.action.ActionContext;

public class VerifyFieldChangedDuringTransitionRoutine extends AbstractRoutine<MutableBoolean> {
	private static final Log LOG = LogFactory.getLog(VerifyFieldChangedDuringTransitionRoutine.class);

	private static SILType[] types = { TypeInst.STRING };

	public VerifyFieldChangedDuringTransitionRoutine(String name) {
		super(name, types, false);
	}
	

	@Override
	protected SILValue<MutableBoolean> executeRoutine(SILContext ctx, List<SILValue<?>> params) {
		MutableIssue issue = (MutableIssue) ctx.getMetaInformation("issue");
		if (issue == null) {
			String msg = String.format(
					"The %s routine can only be used in validatorsand some postfunctions (initial, the very first).",
					new Object[] { getName() });
			LOG.error(msg);
			throw new SILException(msg);
		}
		SILValue ret = SILValueFactory.bool();
		String key = ((SILValue) params.get(0)).toStringValue();
		if (LOG.isDebugEnabled()) {
			LOG.debug(String.format("Modified fields: %s", new Object[] { issue.getModifiedFields().keySet() }));
			String msg = "";
			for (Iterator localIterator = ActionContext.getSingleValueParameters().keySet().iterator(); localIterator
					.hasNext();) {
				Object k = localIterator.next();
				msg = msg.concat(String.format("%s = %s, ",
						new Object[] { k, ActionContext.getSingleValueParameters().get(k).toString() }));
			}
			LOG.debug(msg);
		}

		if (StringUtils.trim(key) == null) {
			String msg = "Cannot check the input for a null issue.";
			LOG.error(msg);
			return ret;
		}

		//
		key = FieldResolver.getFieldId(key, issue.getKey());
		
		Map<String, ModifiedValue> modifiedFields = issue.getModifiedFields();
		if (!modifiedFields.containsKey(key)) {
			return ret;
		}
		// Check if value changed
		ModifiedValue modifiedValue = issue.getModifiedFields().get(key);
		if (Objects.equal(modifiedValue.getOldValue(), modifiedValue.getNewValue())) {
			return ret;
		}

		return SILValueFactory.bool(true);
	}


	public String getParams() {
		return "(\"field\")";
	}

	public SILType<MutableBoolean> getReturnType() {
		return TypeInst.BOOLEAN;
	}


	

	

}
