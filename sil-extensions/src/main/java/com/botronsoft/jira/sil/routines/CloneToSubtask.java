package com.botronsoft.jira.sil.routines;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.user.ApplicationUser;
import com.keplerrominfo.common.util.MutableString;
import com.keplerrominfo.common.util.StringUtils;
import com.keplerrominfo.sil.lang.AbstractRoutine;
import com.keplerrominfo.sil.lang.SILContext;
import com.keplerrominfo.sil.lang.SILType;
import com.keplerrominfo.sil.lang.SILValue;
import com.keplerrominfo.sil.lang.SILValueFactory;
import com.keplerrominfo.sil.lang.type.TypeInst;

public class CloneToSubtask extends AbstractRoutine<MutableString> {
	private static final Log LOG = LogFactory.getLog(CloneToSubtask.class);

	private static final SILType[][] TYPES = { { TypeInst.STRING, TypeInst.STRING },
			{ TypeInst.STRING, TypeInst.STRING, TypeInst.STRING } };

	public CloneToSubtask(String name) {
		super(name, TYPES);
	}

	@SuppressWarnings("unchecked")
	protected SILValue<MutableString> executeRoutine(SILContext ctx, List<SILValue<?>> params) {
		String issueKey = StringUtils.trim(((SILValue) params.get(0)).toStringValue());
		MutableIssue parentIssue = null;
		if (issueKey == null) {
			LOG.error("Cannot clone a null issue!");
			return SILValueFactory.string("");
		}
		parentIssue = ComponentAccessor.getIssueManager().getIssueObject(issueKey);
		if (parentIssue == null) {
			LOG.error("Cannot clone a non-existing issue with key " + issueKey);
			return SILValueFactory.string("");
		}
		//
		String issueTypeName = StringUtils.trim(((SILValue) params.get(1)).toStringValue());
		IssueType issueType = null;
		if (issueTypeName == null) {
			LOG.error("Cannot cerate a subtask with null issueType!");
			return SILValueFactory.string("");
		} else {
			issueType = getIssueType(issueTypeName, parentIssue);
		}
		if (issueType == null) {
			LOG.error("Cannot clone issue as there is no valid issue type with name " + issueTypeName);
			return SILValueFactory.string("");
		}

		String summary = null;
		if (params.size() > 2) {
			summary = StringUtils.trim(((SILValue) params.get(2)).toStringValue());
			if (summary == null) {
				LOG.error("Cannot cerate a subtask with null summary!");
				return SILValueFactory.string("");
			}
		}

		Issue clonedIssue = null;
		try {
			clonedIssue = clone(parentIssue, issueType, summary);
			ComponentAccessor.getSubTaskManager().createSubTaskIssueLink(parentIssue, clonedIssue, getLoggedInUser());
		} catch (Exception e) {
			LOG.error("Could not clone issue, reason follows:", e);
			return SILValueFactory.string("");
		} 

		List createdIssues = (List) ctx.getMetaInformation("created.issues");
		if (createdIssues == null) {
			createdIssues = new ArrayList();
			ctx.setMetaInformation("created.issues", createdIssues);
		}
		createdIssues.add(clonedIssue);

		if (LOG.isDebugEnabled()) {
			LOG.debug(String.format("Issue cloned to subtask >>%s<<", new Object[] { clonedIssue.getKey() }));
		}
		return SILValueFactory.string(clonedIssue.getKey());
	}

	private IssueType getIssueType(String issueType, Issue parent) {
		Collection<IssueType> issueTypes = ComponentAccessor.getIssueTypeSchemeManager()
				.getIssueTypesForProject(parent.getProjectObject());
		for (IssueType sit : issueTypes) {
			if (sit.isSubTask() && sit.getName().equals(issueType)) {
				return sit;
			}
		}
		return null;
	}

	private Issue clone(Issue parentIssue, IssueType issueType, String summary) throws Exception {
		IssueManager issueManager = ComponentAccessor.getIssueManager();
		IssueFactory issueFactory = ComponentAccessor.getIssueFactory();

		MutableIssue newIssue = issueFactory.cloneIssue(parentIssue);
		setNotCloningFieldsToBlankByDefault(newIssue);
		cloneSomeFieldsFromOriginalIssue(newIssue, parentIssue);
		newIssue.setIssueTypeObject(issueType);
		newIssue.setParentObject(parentIssue);
		if (summary != null) {
			newIssue.setSummary(summary);
		} else {
			newIssue.setSummary(parentIssue.getSummary());
		}

		Issue createdIssue = issueManager.createIssueObject(getLoggedInUser(), newIssue);

		return createdIssue;
	}

	public String getParams() {
		return "(parentIssueKey , issueType [,summary] )";
	}

	public SILType<MutableString> getReturnType() {
		return TypeInst.STRING;
	}

	private void setNotCloningFieldsToBlankByDefault(MutableIssue newIssue) {
		newIssue.setCreated(null);
		newIssue.setUpdated(null);
		newIssue.setVotes(null);
		newIssue.setWatches(0L);
		newIssue.setStatusObject(null);
		newIssue.setWorkflowId(null);
		newIssue.setEstimate(0l);
		newIssue.setTimeSpent(null);
		newIssue.setResolutionDate(null);
	}

	/**
	 * Please be informed that there ain't a 100% copy from original issue, but
	 * with modification.
	 */
	private void cloneSomeFieldsFromOriginalIssue(MutableIssue newIssue, Issue originalIssue) {
		newIssue.setFixVersions(withoutArchivedVersions(originalIssue.getFixVersions()));
		newIssue.setAffectedVersions(withoutArchivedVersions(originalIssue.getAffectedVersions()));

		// Retrieve custom fields for the issue type and project of the clone
		// issue (same as original issue)
		List<CustomField> customFields = getCustomFields(originalIssue);

		for (final CustomField customField : customFields) {
			// Set the custom field value of the clone to the value set in the
			// original issue
			Object value = customField.getValue(originalIssue);
			if (value != null) {
				newIssue.setCustomFieldValue(customField, value);
			}
		}
	}

	private List<CustomField> getCustomFields(Issue originalIssue) {
		return ComponentAccessor.getCustomFieldManager().getCustomFieldObjects(originalIssue);
	}

	private Collection<Version> withoutArchivedVersions(Collection<Version> versions) {
		List<Version> notArchivedVersions = new ArrayList<Version>();
		for (final Version version : versions) {
			if (!version.isArchived()) {
				notArchivedVersions.add(version);
			}
		}
		return notArchivedVersions;
	}

	private ApplicationUser getLoggedInUser() {
		return ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
	}
}
